package gryazin.interview.hellojson;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.JsonReader;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import gryazin.interview.hellojson.*;//for AIDE

public class Shower extends Activity
{
   private static final String TAG = "BAGSHOW"; // error log
   
   // constants for saving slideshow state when config changes
   private static final String IMAGE_INDEX = "IMAGE_INDEX";
   private static final String SLIDES_NUMBER = "SLIDES_NUMBER";
   private static final String LON = "LON";
   private static final String LAT = "LAT";
   private static final String RETRY = "RETRY";
   private static final String COUNT = "COUNT";
   private static final String DELAY = "DELAY";
   private static final String START = "START";
   private static final String PHOTOS = "PHOTOS";
   
   private static final int MAX_RETRIES = 5; //How many times to try to find enough photos
   private ImageView slideView; // displays the current image
   private Handler handler; // used to update the slideshow
   private RetainedFragment mWorkFragment;
   private List photos;
   private Drawable rect;
   private List bitmaps;
   
   int nextItemIndex; // index of the next image to display
   int numberOfSlides;
   double lon;
   double lat;
   int delay; //in ms
   int retry;// used in makePanoUrl to make the region wider
   int count;//how many photos are available on that link?
   boolean started;
   final static double diff = 0.002;
   
   
   
   
   
   @Override
   public void onCreate(Bundle savedInstanceState)
   {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.shower);
      slideView = (ImageView) findViewById(R.id.ivSlide);
      rect = getResources().getDrawable(R.drawable.rect);
      
      FragmentManager fm = getFragmentManager();
      mWorkFragment = (RetainedFragment)fm.findFragmentByTag("work");

      if (mWorkFragment == null) {
          mWorkFragment = new RetainedFragment();
          fm.beginTransaction().add(mWorkFragment, "work").commit();
      }
           
      if (savedInstanceState == null) // Activity starting
      {  
    	  nextItemIndex = 0; // start from first image
    	  retry = 1;// used in makePanoUrl to make the region wider
    	  count = 0;
    	  started = false;
    	  bitmaps = new ArrayList<BitmapDrawable>();
    	  mWorkFragment.mbitmaps = bitmaps; //saving!
      } 
      else // Activity resuming
      {
          nextItemIndex = savedInstanceState.getInt(IMAGE_INDEX);
          numberOfSlides = savedInstanceState.getInt(SLIDES_NUMBER);
          lon = savedInstanceState.getDouble(LON); 
          lat = savedInstanceState.getDouble(LAT);
          retry = savedInstanceState.getInt(RETRY);
          count = savedInstanceState.getInt(COUNT);
          delay = savedInstanceState.getInt(DELAY);
          started = savedInstanceState.getBoolean(START);
          photos = mWorkFragment.mslides;
          bitmaps = mWorkFragment.mbitmaps;
      }       
      
      ///WOWOWOW!!! GET INTENT HERE!!
      Intent intent = getIntent();
      if(intent!=null){
    	  lon = intent.getDoubleExtra(MainActivity.LON, -73.964063);
    	  lat = intent.getDoubleExtra(MainActivity.LAT, 40.792308);
    	  numberOfSlides = intent.getIntExtra(MainActivity.NUMBER, 20);
    	  delay = intent.getIntExtra(MainActivity.DELAY, 2500); 	  
      }
   
      handler = new Handler(); 
   } 

   
   @Override
   protected void onStart()
   {
      super.onStart();
     if(!started) {
    	 mWorkFragment.rfiStart(); 
    	 started = true;
     }
     //this makes the slideshow continue after Activity reconstruction
     else if(bitmaps.size()>0){
    	 makeTransition(nextItemIndex-1);
    	 handler.postDelayed(updateSlideshow, delay);
     }
      
   } 

   @Override
   protected void onPause()
   {
      super.onPause();
   } 

   @Override
   protected void onResume()
   {
      super.onResume();
   } 

   @Override
   protected void onStop()
   {
      super.onStop();
      //Stop the slideshow NOW!! Or it'll go on without watchers
      handler.removeCallbacks(updateSlideshow); 
   } 

   @Override
   protected void onDestroy()
   {
      super.onDestroy();
   }
   
   @Override
   protected void onSaveInstanceState(Bundle outState)
   {
      super.onSaveInstanceState(outState);
      
      // save nextItemIndex and others, complex things are held in mWorkFragment
      outState.putInt(IMAGE_INDEX, nextItemIndex);
      outState.putInt(SLIDES_NUMBER, numberOfSlides);
      outState.putDouble(LON, lon); 
      outState.putDouble(LAT, lat);
      outState.putInt(RETRY, retry);
      outState.putInt(COUNT, count);
      outState.putInt(DELAY, delay);
      outState.putBoolean(START, started);
   } 
    
   // anonymous inner class that control slideshow
   private Runnable updateSlideshow = new Runnable()
   {
	   
	   
      @Override
      public void run()
      {
    	  
    	//  Log.d(TAG, "RUN: item " + nextItemIndex + " of " + photos.size() + " frag="+mWorkFragment);
    	 if ((photos.size()>0)&(nextItemIndex >= photos.size()))
         {
    		 makeTransition(nextItemIndex);
    		 nextItemIndex++;
    		 handler.postDelayed(updateSlideshow, delay);
    		
            //finish(); // return to launching Activity
         }
         else
         {     
            mWorkFragment.litStart();
        	
         }
      }
  }; 
   
  /*
   * This UI-less fragment helps to survive the Activity reconstruction,
   * all asynchronous work is done here
   */
  public static class RetainedFragment extends Fragment {
       ProgressBar mProgressBar;
       int mPosition;
       boolean mReady = false;
       boolean mQuiting = false;
       
       ImageView slides;
       ReadPhotosInfo rfi;
       List rfiResult;
       Shower parent;
       List mslides;
       List mbitmaps;
       
       
        @Override
       public void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           setRetainInstance(true);      
       }

       @Override
       public void onActivityCreated(Bundle savedInstanceState) {
           super.onActivityCreated(savedInstanceState);
           parent = (Shower) getActivity();
           slides = (ImageView) parent.findViewById(R.id.ivSlide);
            
       }

       @Override
       public void onDestroy() {
           super.onDestroy();
       }

       @Override
       public void onDetach() {
           // This fragment is being detached from its activity.  We need
           // to make sure its thread is not going to touch any activity
           // state after returning from this function.
           super.onDetach();
           parent = null;
       }
       
       //Start when we get a List of links and other info
       public void rfiStart() {
    	   rfi = parent.new ReadPhotosInfo(parent.lat, parent.lon, parent.numberOfSlides);
           rfi.execute();
       }
       //Just loading ONE image from net
       public void litStart(){
    	   if(parent!=null){//this should never happen, actually...
    		   LoadImageTask lit = parent.new LoadImageTask();
        	   lit.execute(((PhotoInfo)parent.photos.get(parent.nextItemIndex)).photo_file_url); 
           }
       }
   
   }
   /*
    * The class to get JSON and read it in the List
    * 
    * If the number of photos is too small, It's restarted up to MAX_RETRIES times
    */
   class ReadPhotosInfo extends AsyncTask<Void, Void, List>{
	   
	   int mCount;
	   int mRetry;
	   double mLat;
	   double mLon;
	   int mNumber;
	   double mDiff = Shower.diff;
	   
	   ReadPhotosInfo(double lat, double lon, int number){
		   
		   mLat = lat;
		   mLon = lon;
		   mNumber = number;
		   
		   mCount = 0;
		   mRetry = 1; //because is multiplied
	   }
	   
	   String makePanoUrl(int number, double minx, double maxx, double miny, double maxy){
		    //set=full or public
		    return "http://www.panoramio.com/map/get_panoramas.php?set=full&from=0&to=" + 
		    		number + "&minx="+minx+"&miny="+miny+"&maxx="+maxx+"&maxy="+maxy;
		} 
	   
	   
 		@Override
 		protected List doInBackground(Void... in) {
 			// TODO Auto-generated method stub
 			
 			List mList = null;
 			
 			while(mCount<mNumber){
 				
 				try {
 	 				InputStream httpIn = GetContent();
 	 				if(httpIn == null){//if Http request wasn't successful
 	 					//Don't know what to do here:(
 	 					return null;
 	 				}
 	 				else mList = readJsonStream(httpIn);
 	 			} catch (ClientProtocolException e) {
 	 				// TODO Auto-generated catch block
 	 				e.printStackTrace();
 	 			} catch (IOException e) {
 	 				// TODO Auto-generated catch block
 	 				e.printStackTrace();
 	 			}
 				mRetry++;
 				if(mRetry > MAX_RETRIES) break; //That's enough, there's no photos round here!!!
 			}
 			return mList;
 		}
 		
 		//Http work is handled here
 		private InputStream GetContent() throws ClientProtocolException, IOException{
 			HttpClient client = new DefaultHttpClient();
 			HttpGet httpGet = new HttpGet(makePanoUrl(mNumber, mLon-mDiff*mRetry, mLon+mDiff*mRetry, mLat-mDiff*mRetry, mLat+mDiff*mRetry));
 			//HttpGet httpGet = new HttpGet("http://www.panoramio.com/map/get_panoramas.php?set=public&from=0&to=20&minx=-180&miny=-90&maxx=180&maxy=90&size=medium&mapfilter=true");
 			HttpResponse response = client.execute(httpGet);
 			StatusLine statusLine = response.getStatusLine();
 			int statusCode = statusLine.getStatusCode();
 			if(statusCode == 200){
 			HttpEntity entity = response.getEntity();
 			InputStream content = entity.getContent();
 			return content;			
 			}
 			else{
 			return null;
 			}
 		}
 		
 		public List readJsonStream(InputStream in) throws IOException{
 			JsonReader reader = new JsonReader(new InputStreamReader(in));
 			try{
 				return readPhotosArray(reader);
 			}
 			finally{
 				reader.close();
 			}
 		}

 		private List readPhotosArray(JsonReader reader) throws IOException {
 			// TODO Auto-generated method stub
 			List photoInfos = new ArrayList();
 			reader.beginObject();
 			while(reader.hasNext()){
 				String name = reader.nextName();
 				if(name.equals(PhotoInfo.COUNT)){
 					mCount = reader.nextInt();
 				} else if(name.equals(PhotoInfo.PHOTOS)){
 					//here we finally start reading the PhotoInfos
 					reader.beginArray();
 					while(reader.hasNext()){
 						photoInfos.add(readPhotoInfo(reader));
 					}
 					reader.endArray();
 				} else {
 					reader.skipValue();
 				}
 			}
 			reader.endObject();
 			if(mCount == 0) return null; //no photos at all!
 			else return photoInfos;
 		}

 		private PhotoInfo readPhotoInfo(JsonReader reader) throws IOException {
 			// TODO Auto-generated method stub
 			
 			//I was too lazy for setters-getters in PhotoInfo
 			PhotoInfo info = new PhotoInfo();
 			reader.beginObject();
 			while(reader.hasNext()){
 				String name = reader.nextName();
 				if(name.equals(PhotoInfo.PHOTO_ID)){
 					info.photo_id = reader.nextInt();
 				} else if(name.equals(PhotoInfo.PHOTO_TITLE)){
 					info.photo_title = reader.nextString();
 				} else if(name.equals(PhotoInfo.PHOTO_URL)){
 					info.photo_url = reader.nextString();
 				} else if(name.equals(PhotoInfo.PHOTO_FILE_URL)){
 					info.photo_file_url = reader.nextString();
 				} else if(name.equals(PhotoInfo.LONGITUDE)){
 					info.longitude = reader.nextDouble();
 				} else if(name.equals(PhotoInfo.LATITUDE)){
 					info.latitude = reader.nextDouble();
 				} else if(name.equals(PhotoInfo.WIDTH)){
 					info.width = reader.nextInt();
 				} else if(name.equals(PhotoInfo.HEIGHT)){
 					info.height = reader.nextInt();
 				} else if(name.equals(PhotoInfo.UPLOAD_DATE)){
 					info.upload_date = reader.nextString();
 				} else if(name.equals(PhotoInfo.OWNER_ID)){
 					info.owner_id = reader.nextInt();
 				} else if(name.equals(PhotoInfo.OWNER_NAME)){
 					info.owner_name = reader.nextString();
 				} else if(name.equals(PhotoInfo.OWNER_URL)){
 					info.owner_url = reader.nextString();
 				} else{
 					reader.skipValue();
 				}
 			}
 			reader.endObject();
 			return info;
 		}

 		@Override
 		protected void onPostExecute(List result) {
 			// TODO Auto-generated method stub
 			super.onPostExecute(result);
 			count = mCount;
 			photos = result;
 			mWorkFragment.mslides = result;
 			
 			if(mCount<mNumber) SaySorry();
 			//Log.d("CHECK", mCount + "found from " + mNumber + " with " + mRetry + " retries");
 			if(mCount>0) handler.post(updateSlideshow);//start the show!
 		}
 		
 		void SaySorry(){
 			if(mCount == 0){
 				Toast.makeText(Shower.this, "No photos at all, sorry.", Toast.LENGTH_SHORT).show();
 				Shower.this.finish();
 			}
 			else Toast.makeText(Shower.this, "Only " + mCount + "photos found", Toast.LENGTH_SHORT).show();
 		}
 		
 		
 	}
   /*
    * This class downloads the image by file_URL and keeps it in the List called bitmaps 
    * 
    */
  class LoadImageTask extends AsyncTask<String, Void, Bitmap>
   {
      @Override
      protected Bitmap doInBackground(String... urls)
      {
     	 String url = urls[0];
          Bitmap mIcon = null;
          try {
              InputStream in = new java.net.URL(url).openStream();
              mIcon = BitmapFactory.decodeStream(in);
          } catch (Exception e) {
              Log.e("Error", e.getMessage());
              e.printStackTrace();
          }
          return mIcon;
      } // end method doInBackground

     @SuppressWarnings("deprecation")
		@Override
      protected void onPostExecute(Bitmap result)
      {
         super.onPostExecute(result);
         BitmapDrawable next = new BitmapDrawable(result);
         next.setGravity(android.view.Gravity.CENTER);
         nextItemIndex++;
         makeTransition(next);         
         handler.postDelayed(updateSlideshow, delay);
      }  
   } // end class LoadImageTask 
   
  /*
   * Two methods to make a transition of images. 
   * The first is for a new image, the seconds takes it from bitmaps List
   */
   
  //called after downloading
   void makeTransition(BitmapDrawable next){
	   bitmaps.add(next);
	   Drawable previous = slideView.getDrawable();
       
       // if previous is a TransitionDrawable, 
       // get its second Drawable item
       if (previous instanceof TransitionDrawable)
          previous = ((TransitionDrawable) previous).getDrawable(1);
       
       if (previous == null)
          slideView.setImageDrawable(next);
       else
       {
          Drawable[] drawables1 = { previous, rect };
          Drawable[] drawables2 = { rect, next };
          TransitionDrawable transition = new TransitionDrawable(drawables1);
          slideView.setImageDrawable(transition);
          transition.startTransition(delay/10);
          transition = new TransitionDrawable(drawables2);
          slideView.setImageDrawable(transition);
          transition.startTransition(delay/10);
       }
   }
   //called in OnStart to restart the last slide and to make a cycle 
   void makeTransition(int index){
	   //don't want to call mod:D 
	   if(index>=photos.size()){
		   index = index - (nextItemIndex/photos.size())*photos.size();
	   }
	   Drawable previous = slideView.getDrawable();
       BitmapDrawable next = (BitmapDrawable) bitmaps.get(index);
       // if previous is a TransitionDrawable, 
       // get its second Drawable item
       if (previous instanceof TransitionDrawable)
          previous = ((TransitionDrawable) previous).getDrawable(1);
       
       if (previous == null)
          slideView.setImageDrawable(next);
       else
       {
          Drawable[] drawables1 = { previous, rect };
          Drawable[] drawables2 = { rect, next };
          TransitionDrawable transition = new TransitionDrawable(drawables1);
          slideView.setImageDrawable(transition);
          transition.startTransition(delay/10);
          transition = new TransitionDrawable(drawables2);
          slideView.setImageDrawable(transition);
          transition.startTransition(delay/10);
       }
   }
} 
