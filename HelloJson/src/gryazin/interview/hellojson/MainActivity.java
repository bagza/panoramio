package gryazin.interview.hellojson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;
import gryazin.interview.hellojson.*;

public class MainActivity extends Activity {

	final static String BUG = "Main Activity debug"; //for Log.d
	//final static String INFO = "PHOTO_INFOS";
	
	//for intent extras
	final static String DELAY = "DELAY";
	final static String NUMBER = "NUMBER";
	final static String LON = "LON";
	final static String LAT = "LAT";
	
	TextView tvInfo;
	
	
	double lon;
	double lat;
	boolean gps;
	int delay=500;
	int number=1;
	
	private SeekBar sbDelay;
	private SeekBar sbNumber;
	private TextView tvDelay;
	private TextView tvNumber;
	private Button btnShow;
	//DEBUG ONLY
	
	
	LocationManager locationManager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		tvDelay = (TextView) findViewById(R.id.tvDelay);
	    tvNumber = (TextView) findViewById(R.id.tvNumber);
	    sbDelay = (SeekBar) findViewById(R.id.sbDelay);
	    sbNumber = (SeekBar) findViewById(R.id.sbNumber);
	    btnShow = (Button) findViewById(R.id.btnShow2);
	    btnShow.setClickable(false);
    	btnShow.setTextColor(Color.GRAY);
    	
	    sbDelay.setOnSeekBarChangeListener(mySeekBarListener);
	    sbNumber.setOnSeekBarChangeListener(mySeekBarListener);
	    
	    //badly:(
	    if (savedInstanceState == null) // Activity starting
	      {  
	    	  delay = 500;
	    	  number = 1;
	      } 
	      else // Activity resuming
	      {
	          delay = savedInstanceState.getInt(DELAY);
	          number = savedInstanceState.getInt(NUMBER);
	      }
	      
	    
	    locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
	}
	
	OnSeekBarChangeListener mySeekBarListener = new OnSeekBarChangeListener(){

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			// TODO Auto-generated method stub
			switch(seekBar.getId()){
			case R.id.sbDelay:
				delay = 500 + 1500*progress/100;
				tvDelay.setText(String.format("%.1f", delay/1000.0) + "s" );
				break;
			case R.id.sbNumber:	
				number = 1 + 99*progress/100;
				if(number == 1 )tvNumber.setText(number + " photo");
				else tvNumber.setText(number + " photos");
				break;
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			
		}
		
	};
	
	public void onClick(View v){
		switch(v.getId()){
		case R.id.btnShow:
			Intent playShow = new Intent(MainActivity.this, Shower.class);
			playShow.putExtra(NUMBER, number);
			playShow.putExtra(DELAY, delay);
			startActivity(playShow);
			break;
		case R.id.btnShow2:
			playShow = new Intent(MainActivity.this, Shower.class);
			playShow.putExtra(NUMBER, number);
			playShow.putExtra(DELAY, delay);
			playShow.putExtra(LON, lon);
			playShow.putExtra(LAT, lat);
			startActivity(playShow);
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000 * 10, 10, locationListener);
		checkEnabled();
	}
	
	@Override
	  protected void onPause() {
	    super.onPause();
	    locationManager.removeUpdates(locationListener);
	}
	
	private LocationListener locationListener = new LocationListener() {

	    @Override
	    public void onProviderDisabled(String provider) {
	      checkEnabled();
	    }

	    @Override
	    public void onProviderEnabled(String provider) {
	      checkEnabled();
	    }

	    @Override
	    public void onStatusChanged(String provider, int status, Bundle extras) {
	    	if((provider==LocationManager.GPS_PROVIDER)&&(status==LocationProvider.AVAILABLE)){
	    		btnShow.setClickable(true);
		    	btnShow.setTextColor(Color.BLACK);
	    	}
	    	else{
	    		btnShow.setClickable(false);
		    	btnShow.setTextColor(Color.GRAY);
	    	}
	    }

	    @Override
	    public void onLocationChanged(Location location) {
	      handleLocation(location);
	    }
	  };
	  
	private void checkEnabled() {
		    gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
		    	||locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		  }
	
	 private void handleLocation(Location location) {
		    if (location == null){
		    	//btnShow.setClickable(false);
		    	//btnShow.setTextColor(Color.GRAY);
		    }
		    else{
		    	btnShow.setClickable(true);
		    	btnShow.setTextColor(Color.BLACK);
		    	lat = location.getLatitude();
		    	lon = location.getLongitude();
		    }
		  }

		 
}
