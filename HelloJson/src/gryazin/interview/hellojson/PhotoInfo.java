package gryazin.interview.hellojson;

public class PhotoInfo {
	final static String COUNT = "count";
	final static String PHOTOS = "photos";
	
	final static String PHOTO_ID = "photo_id";
	final static String PHOTO_URL = "photo_url";
	final static String PHOTO_TITLE = "photo_title";
	final static String PHOTO_FILE_URL = "photo_file_url";
	final static String LONGITUDE = "longitude";
	final static String LATITUDE = "latitude";
	final static String WIDTH = "width";
	final static String HEIGHT = "height";
	final static String UPLOAD_DATE = "upload_date";
	final static String OWNER_ID = "owner_id";
	final static String OWNER_NAME = "owner_name";
	final static String OWNER_URL = "owner_url";
	
	//i'm too lazy to make setters-getters:(
	public int photo_id;
	public String photo_url;
	public String photo_title;
	public String photo_file_url;
	public Double longitude;
	public Double latitude;
	public int width;
	public int height;
	public String upload_date;
	public int owner_id;
	public String owner_name;
	public String owner_url;
	
	
	
}
